<!-- <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script> -->
  <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
  <script>
  $(document).ready(function() {
    $('.datatab').DataTable();
  } );
  </script>
<footer class="page-footer teal accent-4">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <!--                <h5 class="white-text">Footer Content</h5>-->

                <img id="logo" style="width: 250px; height:200px;" src="img/Abah.png">
                <p class="grey-text text-lighten-4 h3">AWANG YACOUB LUTHMAN MEDIA CENTER 2020</p>
                <ul>
                    <li class="grey-text text-lighten-4">Jalan Imam Bonjol - Melayu</li>
                    <li class="grey-text text-lighten-4">Tenggarong, Kutai Kartanegara</li>
                    <li class="grey-text text-lighten-4">Kalimantan Timur 74415</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright ">
        <div class="container ">
            &copy;AWANG YACOUB LUTHMAN MEDIA CENTER
            <?php echo date('Y'); ?>
            <a class="grey-text text-lighten-4 right waves-effect waves-light" href="#">By.Lukmanul Hakim</a>
        </div>
    </div>
</footer>
</body>

</html>