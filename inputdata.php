<?php require_once "view/header.php"; ?>
<?php
session_start();
if ($_SESSION['status'] != "login") {
  header("location:../quickcount/login.php?pesan=belum_login");
}
?>
<?php
include("koneksi2.php");
?>

<div class="navbar-fixed">
  <nav class="teal accent-4">
    <!-- Menu Navbar -->
    <div class="container">
      <div class="nav-wrapper">
        <a href="inputdata.php" class="brand-logo">Menu Input Data Suara<i class="material-icons left">save</i></a>
        <!-- Menu Desktop -->
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="admin/index.php">Kembali Ke Menu<i class="material-icons left">business</i></a></li>
        </ul>

      </div>
    </div>
  </nav>
  <!-- Tutup Navbar -->
</div>
<!-- Navbar Fixed -->

<?php include "hitungTps.php"; ?>
<div class="row"></div>
<div class="text-lighten-5 container">
  <h4>Silahkan Masukkan Data Suara Dengan Benar</h4>
</div>
<?php include "simpanData.php"; ?>
<!-- FORM -->
<div class="container">
  <div class="row">
    <form class="col m12 s12 l12 container collection with-header" action="simpan.php" method="post" enctype="multipart/form-data">
      <table class="responsive-table">
        <thead>
          <tr>
            <th>Kecamatan</th>
            <th>Nomor TPS</th>
            <th>Nama Desa</th>
          </tr>
        </thead>
        <tbody>
          <tr>
          </tr>
          <tr>

            <img src="asset/img/loading.gif" width="35" id="load1" style="display:none;" />
            <td>

              <div class="input-field col s12">
                <?php
                $sql_provinsi = mysqli_query($con, "SELECT * FROM districts WHERE regency_id =6403 ORDER BY name ASC");
                ?>
                <select class="form-control" name="kecamatan" id="kecamatan">
                  <option></option>
                  <?php
                  while ($rs_provinsi = mysqli_fetch_assoc($sql_provinsi)) {
                    echo '<option value="' . $rs_provinsi['id'] . '">' . $rs_provinsi['name'] . '</option>';
                  }
                  ?>
                </select>
                <img src="asset/img/loading.gif" width="35" id="load1" style="display:none;" />
              </div>
            </td>
            <td>
              <div class="input-field col s12">
                <input placeholder="Nomor TPS" id="tps" name="tps" type="number" min="1" class="validate" required>
              </div>
            </td>
            <td>
              <div class="input-field col s12">
                <!-- <input placeholder="Nama Desa" id="desa" name="desa" type="text" class="validate" required> -->
                <select class="form-control" name="kelurahan" id="kelurahan">>
                  <option></option>
                </select>
                <img src="asset/img/loading.gif" width="35" id="load2" style="display:none;" />
              </div>

            </td>
          </tr>
        </tbody>
        <thead>
          <tr>
            <th>AYL - AYL</th>
            <th>AWANG - YACOUB</th>
            <th>MEDIA - CENTER</th>
          </tr>
        </thead>
        <tbody>
          <tr>
          </tr>
          <tr>
            <td>
              <div class="input-field col s12">
                <input placeholder="Suara Paslon Nomor 1" id="AYL" type="number" min="0" name="paslon1" class="validate" required>
              </div>
            </td>
            <td>
              <div class="input-field col s12">
                <input placeholder="Suara Paslon Nomor 2" id="AWANG" name="paslon2" type="number" min="0" class="validate" required>
              </div>
            </td>
            <td>
              <div class="input-field col s12">
                <input placeholder="Suara Paslon Nomor 3" id="MEDIA" name="paslon3" type="number" min="0" class="validate" required>
                </di>
            </td>
          </tr>
        </tbody>

        <thead>

          <tr>
            <th>SCAN C1</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input type="file" name="scanc1">
            </td>
          </tr>
        </tbody>
      </table>
  </div>
  <div class="row">
    <div class="col s6">
    <input type="submit" name="" value="Upload">
      
    </div>
    <div class=" col s6 text-lighten-5">
      <h5>Jumlah TPS Disimpan : <?php echo $jumlahTPSMasuk; ?> dari 499</h5>
    </div>
  </div>
</div>
</form>
</div>
</div>
<!-- Tutup Form -->
<!-- MODAL DAFTAR GAGAL -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h5>MAAF, DATA TPS SUDAH ADA !!!</h5>
    <div class="divider"></div>
    <p>Data Suara : TPS <?php echo "$tps"; ?></p>
    <p>Desa : <?php echo "$desa"; ?></p>
    <p>Kecamatan : <?php echo "$kecamatan"; ?></p>
    <p><b>SUDAH ADA</b> dalam database.</p>
  </div>
  <div class="modal-footer">
    <a href="inputdata.php" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
  </div>
</div>
<!-- MODAL FORM TIDAK BOLEH KOSONG -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h5>MAAF, FORM TIDAK BOLEH KOSONG !!!</h5>
    <div class="divider"></div>
    <p>Mohon Pastikan Semua Form Sudah Terisi Dengan Benar.</p>
  </div>
  <div class="modal-footer">
    <a href="inputdata.php" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
  </div>
</div>
<!-- MODAL DAFTAR BERHASIL -->
<div id="modal3" class="modal">
  <div class="modal-content">
    <h5>SUKSES !!!</h5>
    <div class="divider"></div>
    <p>Data <b>BERHASIL DISIMPAN</b> Ke Database</p>
  </div>
  <div class="modal-footer">
    <a href="inputdata.php" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
  </div>
</div>


<!-- MODAL PERINGATAN DB -->
<div id="modal4" class="modal">
  <div class="modal-content">
    <h5>SUKSES !!!</h5>
    <div class="divider"></div>
    <p>Data <b>BERHASIL DISIMPAN</b> Ke Database</p>
  </div>
  <div class="modal-footer">
    <a href="inputdata.php" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
  </div>
</div>
<?php require_once "view/footer.php" ?>