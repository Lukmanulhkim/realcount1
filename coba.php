<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Upload Foto</title>
    <style media="screen">
      .link{
        font-family: sans-serif ;
        color: blue;
      }
    </style>
  </head>
  <body>
   
    <h2>Data Foto</h2>
    <table width="100%">
      <tr>
        <td>No</td>
        <td>Foto</td>
        <td></td>
      </tr>
      <?php
      require_once "koneksi3.php";
      $no = 1;
      $query = $conn->query("SELECT * FROM datamasuk");
      while ($data = $query->fetch_assoc()): ?>
        <tr>
          <td><?php echo $no++ ?></td>
          <td>
            <img src="img/<?php echo $data['scanc1'] ?>" alt="" width="50px" height="50px">
          </td>
          <td>
            <a href="editcoba.php?id=<?php echo $data['id'] ?>">Edit</a> ||
            <a href="delete.php?id=<?php echo $data['id'] ?>">Delete</a>
          </td>
        </tr>
      <?php endwhile; ?>
    </table>
    <h2>Upload Foto</h2>
    <form action="simpan.php" method="post" enctype="multipart/form-data">
      <table>
        <tr>
          <td>Pilih Foto</td>
          <td>:</td>
          <td> <input type="file" name="scanc1" value=""> </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td>
            <input type="submit" name="" value="Upload">
            <input type="reset" name="" value="Reset">
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>