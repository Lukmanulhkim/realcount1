<?php require_once "view/header.php"; ?>
<?php require_once "view/navbar.php"; ?>
<div class="row">
  <!-- cek apakah sudah login -->
  <?php
  session_start();
  if ($_SESSION['status'] != "login") {
    header("location:../login.php?pesan=belum_login");
  }
  ?>
  <h4>Selamat datang, <?php echo $_SESSION['username']; ?>! anda telah login.</h4>

  <br />

</div>
<div class="container">
  <fieldset>
    <legend>
      <h4>Pilih Menu</h4>
    </legend>
    <div class="collection">
      <a href="../inputdata.php" class="collection-item">INPUT DATA SUARA</a>
      <a href="../listdata.php" class="collection-item">CARI DATA SUARA</a>
      <a href="../editdata.php" class="collection-item">EDIT DATA SUARA</a>
      <a href="../index.php" class="collection-item">DASHBOARD QUICK COUNT</a>
      <a href="../admin/logout.php" class="collection-item">LOGOUT</a>
    </div>
  </fieldset>
</div>
<div class="row"></div>
<?php require_once "view/footer.php"; ?>