-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2020 at 08:37 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_realcount`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(125) DEFAULT NULL,
  `password` varchar(125) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `datamasuk`
--

CREATE TABLE `datamasuk` (
  `id` int(2) NOT NULL,
  `kecamatan` varchar(60) NOT NULL,
  `desa` varchar(60) NOT NULL,
  `tps` int(60) NOT NULL,
  `paslon_satu` int(30) NOT NULL,
  `paslon_dua` int(30) NOT NULL,
  `paslon_tiga` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datamasuk`
--

INSERT INTO `datamasuk` (`id`, `kecamatan`, `desa`, `tps`, `paslon_satu`, `paslon_dua`, `paslon_tiga`) VALUES
(46, 'TENGGARONG', 'SUKARAME', 1, 2000, 100, 222),
(47, 'MUARA JAWA', 'HANDIL', 1, 100, 2100, 300),
(48, 'KOTA BANGUN', 'KOTA BANGUN', 2, 1500, 1600, 1000),
(49, 'MUARA BADAK', 'MUARA', 1, 300, 1000, 200),
(50, 'TENGGARONG SEBERANG', 'PERJIWA', 1, 230, 130, 80),
(52, 'TENGGARONG', 'MELAYU', 2, 1000, 2000, 1000),
(53, 'TENGGARONG', 'MELAYU3', 2, 0, 0, 0),
(54, 'TENGGARONG', 'PERSEBAYA', 34, 121, 1, 45);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datamasuk`
--
ALTER TABLE `datamasuk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datamasuk`
--
ALTER TABLE `datamasuk`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
