<?php require_once "view/header.php"; ?>
<!-- cek apakah sudah login -->
<?php
session_start();
if ($_SESSION['status'] != "login") {
  header("location:../quickcount/login.php?pesan=belum_login");
}
?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="navbar-fixed">
  <nav class="teal accent-4">
    <!-- Menu Navbar -->
    <div class="container">
      <div class="nav-wrapper">
        <a href="listdata.php" class="brand-logo">Menu Edit Data <i class="material-icons left">edit</i></a>
        <!-- Menu Desktop -->
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="admin/index.php">Kembali Ke Home<i class="material-icons left">business</i></a></li>
        </ul>

      </div>
    </div>
  </nav>
  <!-- Tutup Navbar -->
</div>
<!-- Navbar Fixed -->
<?php require_once "core/init.php"; ?>
<!-- ISI KONTENT -->
<br>




  <?php
  include 'koneksi.php';
  ?>
  <div class="col-lg-12" style="padding-top: 20px; padding-bottom: 20px;">
    
    <hr>
      <table class="table table-stripped table-hover datatab">
        <thead>
          <tr>
            <th>No</th>
            <th>Id</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>TPS</th>
            <th>Paslon 1</th>
            <th>Paslon 2</th>
            <th>Paslon 3</th>
            <th>SCAN C1</th>
            <th>Action</th>                         
            <th>Action</th>                         
          </tr>
        </thead>  
        <tbody>
          <?php 
          $query = mysqli_query($con, "SELECT * FROM datamasuk");
          $no = 1;
          while ($data = mysqli_fetch_assoc($query)) 
          {
          ?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $data['id']; ?></td>
              <td><?php echo $data['kecamatan']; ?></td>
              <td><?php echo $data['desa']; ?></td>
              <td><?php echo $data['tps']; ?></td>
              <td><?php echo $data['paslon_satu']; ?></td>
              <td><?php echo $data['paslon_dua']; ?></td>
              <td><?php echo $data['paslon_tiga']; ?></td>
              <td> <a href="#" data-toggle="modal" data-target="#modalc1<?php echo $data['id']; ?>"><img src="img/upload/scanc1/<?php echo $data['scanc1'] ?>" alt="" width="100px" height="100px"></a>
             
              </td>
              <td>
                <!-- Button untuk modal -->
                <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php echo $data['id']; ?>">Edit</a>
                
              </td>
              <td>
              <a href="" data-toggle="modal" data-target="#modaldelete<?php echo $data['id']; ?>"  type="button" class="btn btn-danger" >Delete</a></td>
            </tr>

            <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="modalc1<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <?php
                        $id = $data['id']; 
                        $query_edit = mysqli_query($con, "SELECT * FROM datamasuk WHERE id='$id'");
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>
        <h5 class="modal-title" id="exampleModalLongTitle">Update SCAN C1  <span>ID : <?php echo $row['id']; ?></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form role="form" action="updategambar.php" method="post" enctype="multipart/form-data">
                       
                        <input type="hidden" name="id_mhs" value="<?php echo $row['id']; ?>">
                        
                       
                        <div class="form-group">
                          <label>SCAN C1</label>
                          <img src="img/upload/scanc1/<?php echo $data['scanc1'] ?>" alt="" width="500px" height="500px">      
                        </div>
                        <div class="form-group">
                          <label>SCAN Baru</label>
                          <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
                          <input type="file" required name="scanc1" value="">
                          <input type="hidden" name="foto_lama" value="<?php echo $data['scanc1'] ?>">      
                        </div>
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <?php 
                        }
                        ?>        
                      </form>
      </div>
      
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modaldelete<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <?php
                        $id = $data['id']; 
                        $query_edit = mysqli_query($con, "SELECT * FROM datamasuk WHERE id='$id'");
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>
        <h3 class="modal-title" id="exampleModalLongTitle">APAKAH ANDA SUDAH YAKIN UNTUK MENGHAPUS DATA   <span>ID : <?php echo $row['id']; ?></span></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
     
                       
                        <input type="hidden" name="id_mhs" value="<?php echo $row['id']; ?>">
                       
                        <div class="form-group">
                        <h4> Kecamatan : <?php echo $row['kecamatan']; ?> </h4>
                          
                        </div>
                        <div class="form-group">
                        <h4> Desa : <?php echo $row['desa']; ?> </h4>
                          
                        </div>
                        <div class="form-group">
                        <h4> TPS : <?php echo $row['tps']; ?> </h4>
                          
                        </div>
                        <div class="form-group">
                        <h4> Suara Paslon 1 : <?php echo $row['paslon_satu']; ?> </h4>
                          
                        </div>
                        <div class="form-group">
                        <h4> Suara Paslon 2 : <?php echo $row['paslon_dua']; ?> </h4>
                          
                        </div>
                        <div class="form-group">
                        <h4> Suara Paslon 3 : <?php echo $row['paslon_tiga']; ?> </h4>
                          
                        </div>

                        <div class="form-group">
                          <h4>SCAN C1</h4>
                          <img src="img/upload/scanc1/<?php echo $row['scanc1'] ?>" alt="" width="200px" height="200px">      
                        </div>
                        
                        
                        <div class="modal-footer">  
                        <a href="delete.php?id=<?php echo $row['id'] ?>"><button type="submit" class="btn btn-danger">Ya Hapus</button></a>
                          <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
                        </div>
                        <?php 
                        }
                        ?>        
                      
      </div>
      
    </div>
  </div>
</div>
            <!-- Modal Edit Mahasiswa-->
            <div class="modal fade" id="myModal<?php echo $data['id']; ?>" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Data TPS</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="proses_edit.php" method="post" enctype="multipart/form-data">
                        <?php
                        $id = $data['id']; 
                        $query_edit = mysqli_query($con, "SELECT * FROM datamasuk WHERE id='$id'");
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>
                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                        <div class="form-group">
                          <label>Kecamatan</label>
                          <input type="text" name="kecamatan" class="form-control" value="<?php echo $row['kecamatan']; ?>">      
                        </div>
                        <div class="form-group">
                          <label>Desa</label>
                          <input type="text" name="desa" class="form-control" value="<?php echo $row['desa']; ?>">      
                        </div>
                        <div class="form-group">
                          <label>TPS</label>
                          <input type="number" name="tps" class="form-control" value="<?php echo $row['tps']; ?>">      
                        </div>
                        <div class="form-group">
                          <label>Paslon 1</label>
                          <input type="text" name="paslon1" class="form-control" value="<?php echo $row['paslon_satu']; ?>">      
                        </div>
                        <div class="form-group">
                          <label>Paslon 2</label>
                          <input type="text" name="paslon2" class="form-control" value="<?php echo $row['paslon_dua']; ?>">      
                        </div>
                        <div class="form-group">
                          <label>Paslon 3</label>
                          <input type="text" name="paslon3" class="form-control" value="<?php echo $row['paslon_tiga']; ?>">      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <?php 
                        }
                        ?>        
                      </form>
                  </div>
                </div>
              </div>
            </div>
          <?php               
          } 
          ?>
        </tbody>
      </table>          
  </div>


<?php require_once "view/footer.php" ?>